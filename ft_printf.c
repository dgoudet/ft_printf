/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 11:19:12 by dgoudet           #+#    #+#             */
/*   Updated: 2020/02/01 10:34:36 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include <stdio.h>

static void	print_percent_char(int **i, const char *str, t_point *x)
{
	while (str[**i] && str[**i + 1] == '%' && str[**i] == '%')
	{
		(**i)++;
		if (str[**i] == '%')
		{
			ft_putchar_and_count(str[**i], x);
			(**i)++;
		}
	}
}

static void	ft_detect_percent(int *i, const char *str, t_point *x)
{
	while (str[*i] && str[*i] != '%')
	{
		ft_putchar_and_count(str[*i], x);
		(*i)++;
	}
	if (str[*i] == '%')
	{
		if (str[*i + 1] == '%')
		{
			print_percent_char(&i, str, x);
			while (str[*i] && str[*i] != '%')
			{
				ft_putchar_and_count(str[*i], x);
				(*i)++;
			}
			if (str[*i] == '%' && str[*i + 1] != '%')
				(*i)++;
		}
		else
			(*i)++;
	}
}

static void	ft_format_string(int *i, const char *str, t_point *x)
{
	int j;
	int k;

	j = 0;
	k = 0;
	while (str[*i] && str[*i] != '%' && (!(ft_isalpha(str[(*i) - 1]))))
	{
		j++;
		k++;
		(*i)++;
	}
	if ((str[*i] != '\0' && (!(ft_isalpha(str[*i]))) && str[*i] != '%')
			|| (ft_isalpha(str[*i]) && ft_isalpha(str[*i - 1])))
	{
		(*i)--;
		j--;
	}
	if (k > 0)
	{
		x->format = ft_substr(str, (*i - j), (j + 1));
		ft_read_format_string(x);
		ft_process_format_string(x);
	}
	if ((str[*i] != '\0' && str[*i] != '%') || x->percent == 1)
		(*i)++;
}

int			ft_printf(const char *str, ...)
{
	t_point		*x;
	int			i;
	int			nb_c;

	if (str == NULL)
		return (-1);
	x = NULL;
	if (!(x = malloc(sizeof(t_point))))
		return (0);
	i = 0;
	x->nb_c = 0;
	va_start(x->ap, str);
	while (str[i])
	{
		ft_detect_percent(&i, str, x);
		if (str[i] != '\0')
			ft_format_string(&i, str, x);
	}
	va_end(x->ap);
	nb_c = x->nb_c;
	free(x);
	x = NULL;
	return (nb_c);
}
