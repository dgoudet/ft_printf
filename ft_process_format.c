/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_process_format.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/01 14:19:39 by dgoudet           #+#    #+#             */
/*   Updated: 2020/02/01 14:19:47 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include <stdio.h>

static void	percent_format(t_point *x)
{
	x->percent = 1;
	ft_print_percent(x);
}

void		ft_process_format_string(t_point *x)
{
	int	i;

	i = 0;
	x->percent = 0;
	while ((!(ft_isalpha(x->format[i])) && x->format[i]) && x->format[i] != '%')
		i++;
	if (x->format[i] == 'c')
		ft_print_c(x);
	else if (x->format[i] == 's')
		ft_print_s(x);
	else if (x->format[i] == 'd' || x->format[i] == 'i' || x->format[i] == 'u')
		ft_print_nb(x, x->format[i]);
	else if (x->format[i] == 'p' || x->format[i] == 'x' || x->format[i] == 'X')
		ft_print_nb(x, x->format[i]);
	else if (x->format[i] == '%')
		percent_format(x);
	else if (x->format[i] != '\0')
	{
		if (x->format[i + 1] == '%')
			percent_format(x);
	}
	else
		ft_putstr_and_count(x->format, x);
	ft_free(x->format);
}
