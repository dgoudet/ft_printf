#include "printf.h"
#include <stdio.h>


#define X(format, ...) \
	printf("\e[1m\e[7m\e[92m\e[107m--------------------------------\e[0m\n"); \
	printf("\e[95m...\e[0m"); \
	r1 = printf(format, ##__VA_ARGS__); \
	fflush(stdout); \
	printf("\e[95m...\e[0m\n\e[95m...\e[0m"); \
	fflush(stdout); \
	r2 = ft_printf(format, ##__VA_ARGS__); \
	printf("\e[95m...\e[0m\n"); \
	fflush(stdout); \
	if (r1 != r2) \
	{ \
		printf("\e[91mReturn ERROR: \e[32m%d \e[97m!= \e[32m%d\e[0m\n", r1, r2); \
		printf("\e[1m\e[7m\e[91m\e[107m--------------------------------\e[0m\n\n\n"); \
	}

int main()
{
	int r1;
	int r2;
	char c = 'a';
	//char *str = "hello";
	//int	nb = -456;

	X("%c", c);
	/*X("9Lw%20.1s5OAjqF93AccPVh", "YM6oQX9bOf2sP0k4a38XaqQMXOCvl7VFnOWa9u1eknFHK5NbOUFI3sy5qcRIL7jgEZQtAKIF1aGw");
	X("4%09XqUh%-.3xB84y%.4u1u", -1206588007, -2147483647, 0);
	X("%03d\n", -12);
	X("%03d\n", 12);
	X("%03.d\n", -12);
	X("%03.1d\n", -12);
	X("%03.3d\n", -12);
	X("%.d", -2);
	X("%0000000*d", 2, 3);
	X("%0000000.00000003d", 3);
	X("%-------3d", 3);
	X("-->|%-4c|<--\n", c);*/
	X("%0000000.00i", 12);
	X("%0000000.01i", 12);
	X("%0000000.02i", 12);
	X("%0000000.03i", 12);
	X("%0000001.00i", 12);
	X("%0000001.01i", 12);
	X("%0000002.02i", 12);
	X("%0000003.03i", 12);
	X("%0000001.00i", 12);
	X("%0000002.01i", 12);
	X("%0000002.02i", 12);
	X("%0000002.03i", 12);
	X("%0000002.04i", 12);
	X("%0000000.00i", -12);
    X("%0000000.01i", -12);
    X("%0000000.02i", -12);
    X("%0000000.03i", -12);
    X("%0000001.00i", -12);
    X("%0000001.01i", -12);
    X("%0000002.02i", -12);
    X("%0000003.03i", -12);
    X("%0000001.00i", -12);
    X("%0000002.01i", -12);
    X("%0000002.02i", -12);
    X("%0000002.03i", -12);
    X("%5.04i%0d", -12, 2);
	/*X("hello%c", c);
	X("pp %.50d\n", 10000);
    X("p1 %.4s\n", "cccc");
	X("p3 %.4s\n", NULL);
	X("p12 %.0d\n", 0);
	X("%%\n");
	X("%d\n", 2147483647);
	X("1hexa-maj 1 %X hexa-maj 2 %X\n\n", 42, -42);
    X("3hexa-maj 1 %   X hexa-maj 2 % X\n\n", 42, -42);
    X("4hexa-maj 1 %12X hexa-maj 2 %int12X\n\n", 42, -42);
    X("4hexa-maj 1 %-12X hexa-maj 2 %-12X\n\n", 42, -42);
    X("5hexa-maj 1 %0X hexa-maj 2 %0X\n\n", 42, -42);
    X("6hexa-maj 1 %012X hexa-maj 2 %012X\n\n", 42, -42);
    X("8hexa-maj 1 %*X hexa-maj 2 %*X\n\n", 42, 6, 6, 6);
	X("4hexa-maj 1 %12X hexa-maj 2 %-12X\n\n", 42, -42);
	X("%%p::[%010d]\n", -8473);*/
    /*X("%%p::[%10d]\n", -8473);
    X("%%p::[%.5d]\n", -8473);
    X("%%p::[%01.1d]\n", -8473);
    X("%%p::[%010.1d]\n", -8473);
	X("%%p::[%010.5d]\n", -8473);
	X("%%p::[%010.6d]\n", -8473);
	X("%%p::[%010.4d]\n", -8473);
    X("%%p::[%01.50d]\n", -8473);
    X("%%p::[%1.50d]\n", -8473);
    X("%%p::[%0100.50d]\n", -8473);
    X("%%p::[%010d]\n", 8473);
    X("%%p::[%10d]\n", 8473);
    X("%%p::[%.5d]\n", 8473);
    X("%%p::[%01.1d]\n", 8473);
    X("%%p::[%010.1d]\n", 8473);
    X("%%p::[%01.50d]\n", 8473);
    X("%%p::[%1.50d]\n", 8473);
    X("%%p::[%0100.50d]\n", 8473);
	X("p29 %.3u\n", 100);
	X("%010%");
	X("%010w%");
	X("%-10w%");
	X("percent 1 %012%");
    X("percent 2 %12%");
    X("percent 3 %-12%");
	X("percent 3 %-012%");
    X("percent 4 %0%");
    X("percent 5 % %");
    X("percent 6 % 15%");
    X("percent 7 % 12%");
    X("percent 8 %  *%", 13);
	X("ultimate2 %*d %*s %*x %*X %*i %*u\n", 1, 5000, 1, "hey", 10, 54700, 1, 300, 10000, -55,     1, -60);
	X("ultimate2 %*d %*s %*x %*X %*i\n", 1, 5000, 1, "hey", 10, 54700, 1, 300, 10000, -55);
	X("ultimate2 %*i %*u\n", 10000, -55, 1, -60);
	X("ultimate2 %*i \n", 10000, -55);*/
	/*X("%.s", "hello");
	X("78L0c%11c7rFII%11s%c", 'U', "", '3');
	X("|%-*.*s|", -3, -3, "test");
	X("|%.p| et |%p|", NULL, "test");
	X("%.s", NULL);
	X("%-7s%5s", "hello", "world");
	X("%7s%-5s", "hello", "world");
	X("%.s", "NULL");
	X("%3.s", NULL);
	X("%.p", NULL);
	X("%p", NULL);
	X("%0*i", -7, -54);
	X("%-7i", -54);
	X("|%*.*d|\n", -45, 45, 0);
	X("|%.*d|\n", -45, 0);
	X("|%*.*d|\n", -45, -45, 0);
	X("|%0.0X|\n", 3);
	X("%.0d", 0);
	X("%.0d", 2);
	X("%.d", 0);
	X("%3.d", 0);
	X("%3.0d", 0);
	X("%0.0d", 0);
	X("percent 8 %10.10%");
	X("%chello", c);
	X("%chello%c", c, 'b');
	X("hop%chello%c%chey%c", c, 'b', 'c', 'd');
	X("%%%c", c);
	X("%4c", c);
	X("%4.c", c);
	X("%-4.c", c);
	X("%*c", 4, c);
	X("bye%-4.chello%%%c", c, 'b');
	X("%s", str);
	X("%shi", str);
	X("hi%s", str);
	X("hi%shi%s", str, str);
	X("%-s", str);
	X("%3s", str);
	X("%6s", str);
	X("%-6s", str);
	X("%.3s", str);
	X("%4.3s", str);
	X("%3.4s", str);
	X("%14.3s", str);
	X("%.12s", "hello I love you so much");
	X("%*.3s", 5, str);
	X("%5.*s", 3, str);
	X("%*.*s", 5, 3, str);
	X("%*.2s", -5, str);
	X("%6.*s", -2, str);
	X("%*.*s", -6, -2, str);
	X("%d%d", 56, -5);
	X("hello%dyo%s", 56, str);
	X("%04d", 56);
	X("%04.3d", 56);
	X("%0d", 56);
	X("%-4d", 56);
	X("%0.1d", 56);
	X("%4.3d", 56);
	X("%3.3d", 56);
	X("%3.4d", 56);
	X("%03.4d", 56);
	X("%-3.4d", 56);
	X("%-4.3d", 56);
	X("%-4.3u", 56);
	X("%x", 56000000);
	X("%X", 56000000);
	X("%0.2x", 56000000);
	X("%15x", 56000000);
	X("%-13x", 56000000);
	X("%p", &str);
	X("%20p", &str);
	X("%-20p", &str);
	X("%d%c", 4, c);
	X("%d%s", 4, str);
	X("bonjour%d%s", 4, str);
	X("%d%sbonjourcsd", 4, str);
	X("%p", str);
	X("%u", nb);
	X("%i", nb);
	X("%x", nb);
	X("%X", nb);
	X("%d%%", nb);
	X("%s", NULL);
	X("%0d", nb);
	X("%4d", nb);
	X("%5d", nb);
	X("%-5d", nb);
	X("%-5c", c);
	X("%-05d", nb);
	X("%0-5d", nb);
	X("%04c", c);
	X("%-04c", c);
	X("%-8.5d", nb);
	X("%-6.5d", nb);
	X("%%d", nb);
	//X(NULL, NULL);
	//X("%-.3d", 3);
	//X("%-.3s", str);
	//char *str = "%.2s %s";
	//char *str1 = "hello";
	//char *str2 = "hi";
	*char *str = "%d%d";
	int nb1 = 1;
	int nb2 = 2;
	//printf("%*s\n", -9, str);
	//printf("%*s\n", 9, str);
	//printf("%4.*s\n", -2, str);
	//ft_printf("%c", c);*/
}
