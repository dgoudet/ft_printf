/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 11:19:24 by dgoudet           #+#    #+#             */
/*   Updated: 2020/02/01 12:32:00 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINTF_H
# define PRINTF_H
# include <unistd.h>
# include <stdarg.h>
# include <string.h>
# include <stdlib.h>

typedef struct		s_point
{
	va_list			ap;
	char			*format;
	int				j;
	int				percent;
	int				c;
	char			*s;
	char			*s_precised;
	long int		n;
	char			*ptr;
	unsigned long	address;
	unsigned int	u;
	unsigned int	un;
	unsigned int	uhlow;
	unsigned int	uhup;
	int				nb_min;
	int				nb_max;
	int				nb_c;
	int				minus;
	int				is_nb_max;
	int				is_zero_flag;
	int				is_address;
	char			*s_zeros;
	int				count;
}					t_point;

int					ft_printf(const char *str, ...);
size_t				ft_strlen(const char *s);
int					ft_isascii(int c);
char				*ft_substr(char const *s, unsigned int start, size_t len);
int					ft_isalpha(int c);
void				ft_process_format_string(t_point *x);
void				ft_print_c(t_point *x);
void				ft_print_s(t_point *x);
void				ft_print_nb(t_point *x, char c);
void				ft_putchar_and_count(char c, t_point *x);
void				ft_read_format_string(t_point *x);
void				ft_putstr_and_count(char *s, t_point *x);
char				*ft_itoa(int n);
void				ft_put_spaces(char *s, t_point *x);
void				ft_width(char *s, t_point *x);
char				*ft_put_zeros(char *string, int size, t_point *x);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_conv_base(unsigned long nb, char *base, t_point *x);
char				*ft_strdup(const char *s1);
void				ft_free(char *s);
void				ft_zero_flag(t_point *x);
void				ft_prepare_s(t_point *x);
void				ft_print_percent(t_point *x);
void				ft_return_nothing(t_point *x);
void				ft_create_s_ptr(t_point *x);
void				ft_create_s_int(t_point *x);
void				ft_prepare_if_neg(int *i, char *str, t_point *x);
#endif
