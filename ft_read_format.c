/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_format.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 16:47:21 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/30 10:58:09 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include <stdio.h>

static void	ft_fill_precision(t_point *x, int *i)
{
	int unit;

	unit = 1;
	while (x->format[*i] >= '0' && x->format[*i] <= '9')
	{
		x->is_nb_max = 1;
		x->nb_max = x->nb_max * unit + (x->format[*i] - 48);
		unit = 10;
		(*i)++;
	}
	if (x->format[*i] == '*')
	{
		x->is_nb_max = 1;
		x->nb_max = va_arg(x->ap, int);
		if (x->nb_max < 0)
		{
			x->is_nb_max = 0;
			x->nb_max = 0;
		}
	}
}

static void	ft_fill_width(t_point *x, int *i)
{
	int unit;

	unit = 1;
	if ((x->format[*i] == '*' && x->format[*i + 1] == '.')
			|| (x->format[*i] == '*' && ft_isalpha(x->format[*i + 1]))
			|| (x->format[*i] == '*' && x->format[*i + 1] == '%'))
	{
		x->nb_min = va_arg(x->ap, int);
		if (x->nb_min < 0)
			x->is_zero_flag = 0;
		if (x->nb_min < 0)
		{
			x->minus = 1;
			x->nb_min = x->nb_min * -1;
		}
	}
	while (x->format[*i] >= '0' && x->format[*i] <= '9')
	{
		x->nb_min = x->nb_min * unit + (x->format[*i] - 48);
		unit = 10;
		(*i)++;
	}
}

static void	ft_init_variables(t_point *x)
{
	x->nb_min = 0;
	x->nb_max = 0;
	x->is_nb_max = 0;
	x->minus = 0;
	x->is_zero_flag = 0;
	x->n = 0;
}

void		ft_read_format_string(t_point *x)
{
	int i;

	i = 0;
	ft_init_variables(x);
	while (x->format[i] && (!(ft_isalpha(x->format[i]))))
	{
		while (x->format[i] == '0')
		{
			x->is_zero_flag = 1;
			i++;
		}
		while (x->format[i] == '-')
		{
			x->minus = 1;
			i++;
		}
		ft_fill_width(x, &i);
		if (x->format[i] == '.')
		{
			i++;
			x->is_nb_max = 1;
			ft_fill_precision(x, &i);
		}
		i++;
	}
}
