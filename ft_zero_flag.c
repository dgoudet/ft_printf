/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_zero_flag.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 16:17:16 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/25 18:59:25 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include <stdio.h>

char		*ft_put_zeros(char *string, int size, t_point *x)
{
	int		i;
	int		j;
	char	*s;

	i = 0;
	j = 0;
	if (!(s = malloc(sizeof(char) * (size + 1))))
		return (NULL);
	ft_prepare_if_neg(&i, s, x);
	while ((size_t)size > ft_strlen(string))
	{
		s[i] = '0';
		i++;
		size--;
	}
	while (string[j])
	{
		s[i] = string[j];
		i++;
		j++;
	}
	s[i] = '\0';
	return (s);
}

static void	ft_apply_zero_flag(t_point *x)
{
	int size;

	if ((size_t)x->nb_max > ft_strlen(x->s))
	{
		x->s_zeros = ft_put_zeros(x->s, x->nb_max, x);
		if (x->nb_max < x->nb_min)
			ft_width(x->s_zeros, x);
		else
			ft_putstr_and_count(x->s_zeros, x);
	}
	else
	{
		if (x->n < 0)
			size = x->nb_min - 1;
		else
			size = x->nb_min;
		x->s_zeros = ft_put_zeros(x->s, size, x);
		ft_putstr_and_count(x->s_zeros, x);
	}
}

static void	ft_case_2(t_point *x)
{
	if ((size_t)x->nb_max > ft_strlen(x->s))
		ft_apply_zero_flag(x);
	else
	{
		if (x->n < 0)
			ft_putchar_and_count('-', x);
		ft_putstr_and_count(x->s, x);
	}
}

static int	ft_define_len(t_point *x)
{
	int len;

	if (x->n < 0)
		len = ft_strlen(x->s) + 1;
	else
		len = ft_strlen(x->s);
	return (len);
}

void		ft_zero_flag(t_point *x)
{
	int len;

	if (x->nb_min > x->nb_max)
	{
		len = ft_define_len(x);
		if (x->nb_min > len && (x->is_nb_max == 0
					|| (size_t)x->nb_max > ft_strlen(x->s)))
			ft_apply_zero_flag(x);
		else
		{
			if (x->nb_min > len)
			{
				ft_prepare_s(x);
				ft_width(x->s, x);
			}
			else
			{
				if (x->n < 0)
					ft_putchar_and_count('-', x);
				ft_putstr_and_count(x->s, x);
			}
		}
	}
	else
		ft_case_2(x);
}
