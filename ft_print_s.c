/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_s.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 16:55:31 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/25 19:20:54 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include <stdio.h>

static int	ft_check_particular_cases(t_point *x)
{
	if (x->is_nb_max == 1 && x->nb_max == 0)
	{
		ft_return_nothing(x);
		return (0);
	}
	if (x->s == NULL)
	{
		ft_free(x->s);
		x->s = ft_strdup("(null)");
	}
	return (1);
}

void		ft_print_s(t_point *x)
{
	x->s_precised = NULL;
	x->s = va_arg(x->ap, char *);
	if (!(ft_check_particular_cases(x)))
		return ;
	if (x->is_nb_max == 1 && (size_t)x->nb_max < ft_strlen(x->s))
	{
		x->s_precised = ft_substr(x->s, 0, x->nb_max);
		if ((size_t)x->nb_min > ft_strlen(x->s_precised))
			ft_width(x->s_precised, x);
		else
			ft_putstr_and_count(x->s_precised, x);
	}
	else if ((x->is_nb_max == 0 ||
				(x->is_nb_max == 1 && ((size_t)x->nb_max >= ft_strlen(x->s))))
			&& ((size_t)x->nb_min > ft_strlen(x->s)))
		ft_width(x->s, x);
	else if (((size_t)x->nb_min <= ft_strlen(x->s)) && (x->is_nb_max == 0
				|| (x->is_nb_max == 1 && (size_t)x->nb_max >= ft_strlen(x->s))))
		ft_putstr_and_count(x->s, x);
	ft_free(x->s_precised);
	ft_free(x->s);
}
