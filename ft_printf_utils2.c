/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_utils2.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/18 16:11:47 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/25 18:20:43 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

static int	len_of_string(int n)
{
	int				len;
	unsigned int	nb;

	if (n < 0)
	{
		nb = n * -1;
		len = 2;
	}
	else
	{
		len = 1;
		nb = n;
	}
	while (nb > 9)
	{
		nb = nb / 10;
		len++;
	}
	return (len);
}

char		*ft_itoa(int n)
{
	char			*str;
	int				len;
	unsigned int	nb;

	len = len_of_string(n);
	if (n < 0)
		nb = n * -1;
	else
		nb = n;
	if ((str = malloc(sizeof(*str) * (len + 1))) == NULL)
		return (NULL);
	str[len] = '\0';
	len--;
	while (len >= 0)
	{
		if (len == 0 && n < 0)
			str[len] = '-';
		else
		{
			str[len] = (nb % 10) + 48;
			nb = nb / 10;
		}
		len--;
	}
	return (str);
}

char		*ft_strdup(const char *s1)
{
	char	*s2;
	int		i;

	i = 0;
	if ((s2 = malloc(sizeof(*s2) * (ft_strlen(s1) + 1))) == NULL)
		return (NULL);
	while (s1[i])
	{
		s2[i] = s1[i];
		i++;
	}
	s2[i] = '\0';
	return (s2);
}

void		ft_free(char *s)
{
	s = NULL;
	free(s);
	s = NULL;
}

int			ft_isalpha(int c)
{
	if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
		return (1);
	else
		return (0);
}
