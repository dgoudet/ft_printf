/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_common_format.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/25 17:49:18 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/25 19:06:29 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include <stdio.h>

void	ft_put_spaces(char *s, t_point *x)
{
	while ((size_t)x->nb_min > ft_strlen(s))
	{
		ft_putchar_and_count(' ', x);
		x->nb_min--;
	}
}

void	ft_width(char *s, t_point *x)
{
	if (x->minus == 1)
	{
		ft_putstr_and_count(s, x);
		ft_put_spaces(s, x);
	}
	else
	{
		ft_put_spaces(s, x);
		ft_putstr_and_count(s, x);
	}
}

void	ft_prepare_s(t_point *x)
{
	char *temp;

	if (x->n < 0)
	{
		temp = ft_strdup(x->s);
		ft_free(x->s);
		x->s = ft_strjoin("-", temp);
		ft_free(temp);
	}
}

void	ft_return_nothing(t_point *x)
{
	ft_free(x->s);
	while (x->nb_min > 0)
	{
		ft_putchar_and_count(' ', x);
		x->nb_min--;
	}
}
