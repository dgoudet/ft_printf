/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_c.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 10:44:18 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/25 17:39:47 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include <stdio.h>

void	ft_print_c(t_point *x)
{
	x->c = va_arg(x->ap, int);
	if (x->nb_min > 1)
	{
		if (x->minus == 1)
		{
			ft_putchar_and_count(x->c, x);
			while (x->nb_min > 1)
			{
				ft_putchar_and_count(' ', x);
				x->nb_min--;
			}
		}
		else
		{
			while (x->nb_min > 1)
			{
				ft_putchar_and_count(' ', x);
				x->nb_min--;
			}
			ft_putchar_and_count(x->c, x);
		}
	}
	else
		ft_putchar_and_count(x->c, x);
}
