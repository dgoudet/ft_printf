/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_nb.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/18 17:29:31 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/25 19:19:47 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "printf.h"

char	*ft_precised(char *string, int size, t_point *x)
{
	int		i;
	char	*s;
	int		len;
	int		temp;

	len = ft_strlen(string);
	i = 0;
	temp = x->nb_max;
	if (!(s = malloc(sizeof(char) * (size + 1))))
		return (NULL);
	s[temp] = '\0';
	temp--;
	len--;
	while (temp >= 0)
	{
		s[temp] = x->s_zeros[len];
		temp--;
		len--;
	}
	return (s);
}

void	ft_create_s(t_point *x, char c)
{
	x->j = 0;
	x->count = 1;
	x->is_address = 0;
	if (c == 'i' || c == 'd')
		ft_create_s_int(x);
	if (c == 'u')
	{
		x->u = va_arg(x->ap, unsigned int);
		ft_conv_base(x->u, "0123456789", x);
	}
	if (c == 'x')
	{
		x->u = va_arg(x->ap, unsigned int);
		ft_conv_base(x->u, "0123456789abcdef", x);
	}
	if (c == 'X')
	{
		x->u = va_arg(x->ap, unsigned int);
		ft_conv_base(x->u, "0123456789ABCDEF", x);
	}
	if (c == 'p')
		ft_create_s_ptr(x);
}

void	ft_print_nb(t_point *x, char c)
{
	ft_create_s(x, c);
	x->s_zeros = NULL;
	x->s_precised = NULL;
	if (x->is_nb_max == 1 && x->nb_max == 0
			&& x->is_address == 0 && x->s[0] == '0')
		ft_return_nothing(x);
	else if (x->is_zero_flag == 1 || ((size_t)x->nb_max > ft_strlen(x->s)))
		ft_zero_flag(x);
	else if ((size_t)x->nb_min > ft_strlen(x->s))
	{
		ft_prepare_s(x);
		ft_width(x->s, x);
	}
	else
	{
		if (x->n < 0)
			ft_putchar_and_count('-', x);
		ft_putstr_and_count(x->s, x);
	}
	ft_free(x->s_zeros);
	ft_free(x->s_precised);
	ft_free(x->s);
}
