/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/25 19:07:56 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/25 19:17:41 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void	ft_str_with_base(char *base, unsigned long nbr, t_point *x)
{
	unsigned int	i;

	i = 0;
	while (i < nbr)
		i++;
	x->s[x->j] = base[i];
	x->j++;
}

void	ft_itoa_base_count(unsigned long nbr, char *base, t_point *x)
{
	unsigned int	size;
	unsigned long	nbu;

	size = 0;
	nbu = nbr;
	size = 0;
	size = ft_strlen(base);
	if (nbu > (size - 1))
	{
		x->count++;
		ft_itoa_base_count(nbu / size, base, x);
	}
}

void	ft_itoa_base(unsigned long nbr, char *base, t_point *x)
{
	unsigned int	size;
	unsigned long	nbu;

	size = 0;
	nbu = nbr;
	size = 0;
	size = ft_strlen(base);
	if (nbu > (size - 1))
		ft_itoa_base(nbu / size, base, x);
	ft_str_with_base(base, nbu % size, x);
}

char	*ft_conv_base(unsigned long nb, char *base, t_point *x)
{
	ft_itoa_base_count(nb, base, x);
	if (!(x->s = malloc(sizeof(char) * (x->count + 1))))
		return (NULL);
	ft_itoa_base(nb, base, x);
	x->s[x->j] = '\0';
	return (x->s);
}
