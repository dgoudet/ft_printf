/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_percent.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/23 11:11:49 by dgoudet           #+#    #+#             */
/*   Updated: 2020/02/01 14:17:57 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include <stdio.h>

static void	ft_put_spaces_percent(t_point *x)
{
	while (x->nb_min > 1)
	{
		ft_putchar_and_count(' ', x);
		x->nb_min--;
	}
	ft_putchar_and_count('%', x);
}

static void	ft_put_spaces_minus_percent(t_point *x)
{
	ft_putchar_and_count('%', x);
	while (x->nb_min > 1)
	{
		ft_putchar_and_count(' ', x);
		x->nb_min--;
	}
}

static void	ft_apply_zero_percent(t_point *x)
{
	while (x->nb_min > 1)
	{
		ft_putchar_and_count('0', x);
		x->nb_min--;
	}
	ft_putchar_and_count('%', x);
}

void		ft_print_percent(t_point *x)
{
	if (x->is_zero_flag == 1 && x->minus == 0)
		ft_apply_zero_percent(x);
	else
	{
		if (x->minus == 1)
			ft_put_spaces_minus_percent(x);
		else
			ft_put_spaces_percent(x);
	}
}
