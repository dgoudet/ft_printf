/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_write_and_count.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 16:32:41 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/25 19:04:00 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void	ft_putchar_and_count(char c, t_point *x)
{
	write(1, &c, 1);
	x->nb_c++;
}

void	ft_putstr_and_count(char *s, t_point *x)
{
	write(1, s, ft_strlen(s));
	x->nb_c = x->nb_c + ft_strlen(s);
}
