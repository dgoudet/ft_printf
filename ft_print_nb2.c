/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_nb2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/25 18:06:56 by dgoudet           #+#    #+#             */
/*   Updated: 2020/01/25 18:48:07 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void	ft_create_s_ptr(t_point *x)
{
	x->is_address = 1;
	x->ptr = va_arg(x->ap, char *);
	if (x->ptr == 0 && x->is_nb_max == 1)
		x->s = ft_strdup("0x");
	else
	{
		x->address = (unsigned long)x->ptr;
		ft_conv_base(x->address, "0123456789abcdef", x);
		x->ptr = ft_strdup(x->s);
		ft_free(x->s);
		x->s = ft_strjoin("0x", x->ptr);
	}
	ft_free(x->ptr);
}

void	ft_create_s_int(t_point *x)
{
	x->n = va_arg(x->ap, int);
	if (x->n < 0)
		x->u = x->n * -1;
	else
		x->u = x->n;
	ft_conv_base(x->u, "0123456789", x);
}
