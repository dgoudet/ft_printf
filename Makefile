SRCS	= ft_printf.c ft_printf_utils.c ft_print_c.c ft_process_format.c ft_write_and_count.c ft_read_format.c ft_print_s.c ft_print_nb.c ft_print_nb2.c ft_common_format.c ft_conv_base.c ft_printf_utils2.c ft_zero_flag.c ft_zero_flag2.c ft_print_percent.c

OBJS	= ${SRCS:.c=.o}

NAME	= libftprintf.a

HEADER	= printf.h

CC	= gcc

RM	= rm -f

CFLAGS	= -Wall -Wextra -Werror

.c.o:
	${CC} -I${HEADER} ${CFLAGS} -c $< -o ${<:.c=.o}

$(NAME):        ${OBJS}
	ar rc ${NAME} ${OBJS} ${HEADER}

all:            ${NAME}

clean:
	${RM} ${OBJS}

fclean:         clean
	${RM} ${NAME}

re:	fclean all

.PHONY:	fclean clean all
